import json

from django.http import JsonResponse
from django.views import View

from core.models import CompressionRequests
from core.serializers import CompressionRequestsSerializer

from django.shortcuts import render_to_response
from django.template.context_processors import csrf

from core import zipper


class Index(View):

    def get(self, request):
        """ Renders the index page. """
        csrf_token = {}
        csrf_token.update(csrf(request))

        return render_to_response('index.html', csrf_token)


class CompressLocalFile(View):
    """ Manages requests for compression. """

    def get(self, request):
        """ Returns all the compression requests and their statuses. """
        compression_requests = CompressionRequests.objects.all()
        serializer = CompressionRequestsSerializer(compression_requests,
                                                   many=True)
        return JsonResponse(serializer.data, status=200, safe=False)

    def post(self, request):
        """ Registers a new compression request. """
        data = json.loads(request.body.decode('utf-8'))
        serializer = CompressionRequestsSerializer(data=data)
        if serializer.is_valid():
            # Save an item in the database so it can be picked up and its
            # status can be tracked.
            serializer.save()
            # Initiate the compression
            compression_executor = zipper.ZipperApi(serializer.data['id'])
            compression_executor.initiate_compression()

            return JsonResponse(serializer.data, status=201)

        return JsonResponse(serializer.errors, status=400)
