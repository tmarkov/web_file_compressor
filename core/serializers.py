from rest_framework import serializers

from core.models import CompressionRequests


class CompressionRequestsSerializer(serializers.Serializer):
    """ Serializer for the "CompressionRequests" model. """
    id = serializers.ReadOnlyField()
    created = serializers.DateTimeField(read_only=True)
    file_location = serializers.CharField()
    is_started = serializers.BooleanField(default=False)
    is_finished = serializers.BooleanField(default=False)

    class Meta:
        model = CompressionRequests
        fields = [
            'id',
            'creted',
            'file_location',
            'is_started',
            'is_finished',
        ]

    def create(self, validated_data):
        """ Create and return a new "CompressionRequests"
        instance, given the validated data.
        """
        return CompressionRequests.objects.create(**validated_data)
