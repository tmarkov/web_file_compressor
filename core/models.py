from django.db import models


class CompressionRequests(models.Model):
    """ Compression requests register. """
    created = models.DateTimeField(auto_now_add=True)
    file_location = models.TextField()
    is_started = models.BooleanField(default=False)
    is_finished = models.BooleanField(default=False)
