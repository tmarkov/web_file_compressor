import asyncio
import threading
import zipfile

from core.models import CompressionRequests
import concurrent.futures


class ZipperApi:
    """ Performs file compression/archiving based on a provided file path.
    The compression is asynchronous. This way after the archiving process
    is started, it can be marked in the database that it has started
    successfully. """

    def __init__(self, target_id):
        self.target_id = target_id
        self.target_file = self._retrieve_target_file()
        self.zipf = None
        self.executor = None

    def initiate_compression(self):
        """ Initiates the compression to the targeted file. """
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        # asyncio executor is required in order to deal with the zipping
        # which is a blocking function.
        self.executor = concurrent.futures.ThreadPoolExecutor(max_workers=5)

        # The compression must be run in a new thread so that it does not
        # stop the Django server.
        t = threading.Thread(target=self._loop_in_thread, args=(loop,))
        t.start()

    def _loop_in_thread(self, loop):
        """ Runs the tasks in the loop.
        :param loop: Loop instance;
        """
        # The loop is reset to this thread.
        asyncio.set_event_loop(loop)
        loop.run_until_complete(asyncio.wait([
            self._executor_func(loop),
            self._mark_started()
        ]))
        self._mark_completed()

    async def _executor_func(self, loop):
        """ This function is created only for synchronous execution of the
        compression.
        """
        await loop.run_in_executor(self.executor, self._compress_with_zip)

    def _compress_with_zip(self):
        """ Creates the zip file and records the compressed data. """
        self.zipf = zipfile.ZipFile("{}.zip".format(self.target_file),
                                    'w',
                                    zipfile.ZIP_DEFLATED)
        self.zipf.write(self.target_file)
        self.zipf.close()

    async def _mark_started(self):
        """ Marks in the database that the compression has started. """
        compression_obj = CompressionRequests.objects.filter(
            pk=self.target_id)
        compression_obj.update(is_started=True)

    def _mark_completed(self):
        """ Marks in the database that the compression has started. """
        compression_obj = CompressionRequests.objects.filter(pk=self.target_id)
        compression_obj.update(is_finished=True)

    def _retrieve_target_file(self):
        """ From the ID of the record, returns the file location from
        the database after processing it into a usable path string.
        :return: The file location;
        """
        compression_rec = CompressionRequests.objects.get(pk=self.target_id)
        # TODO: Create path parser in future
        # target_file_raw = compression_rec.file_location
        # target_file_items = target_file_raw.split("/")
        # target_file = os.path.join(*target_file_items)

        return compression_rec.file_location
