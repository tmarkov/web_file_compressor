/*
 * Gets the CSRF cookie
 */
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}


$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        var csrftoken = getCookie('csrftoken');
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

/*
 * Fetches the user input for the file path.
 */
function getFileLocPath() {
    return $('#exampleFormControlTextarea1').val();
}

/*
 * Sends a request for a new compression depending on the user input.
 */
function sendCompressionRequest() {
    var fileLocation = getFileLocPath();

    $.ajax({
        data: JSON.stringify({file_location: fileLocation}),
        type: 'POST',
        dataType: 'json',
        contentType: "application/json",
        url: "http://localhost:8000/api/v1/compress",
        credentials: 'include',
        success: function () {
            window.alert("Delivered");
            retrieveSyncs();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr);
            console.log(thrownError);
            alert(thrownError);
        }
    });
}


/*
 * Updates the results table with the received compression history.
 * Last one on top.
 */
function updateTableContent(data) {
    var i;
    var table = $('#compressions_status');
    table.empty();
    console.log(data.length);
    for(i = data.length - 1; i >= 0; i--) {
        console.log(data.length);
        console.log(i);
        var row = '<tr>' +
            '<td scope="row">' + data[i].id + '</td>' +
            '<td>' + data[i].created + '</td>' +
            '<td>' + data[i].file_location + '</td>' +
            '<td>' + data[i].is_started + '</td>' +
            '<td>' + data[i].is_finished + '</td>' +
            '</tr>';
        table.append(row);
    }
}


/*
 * Retrieves the compression history from the api.
 */
function retrieveSyncs() {
    $.ajax({
        method: 'GET',
        async: false,
        url: "http://localhost:8000/api/v1/compress",
        dataType: 'json',
        success: function (data) {
            updateTableContent(data);
        },
        error: function () {
            alert("There was an error retrieving information from the API.");
        }
    });
}


$('#submit_btn').click( function () {
    sendCompressionRequest();
});

$('#refresh_btn').click( function () {
    retrieveSyncs();
});

retrieveSyncs();