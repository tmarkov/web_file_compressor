# About
Training application for file compressor with a web interface.

# Technologies
Python 3.6
Django 2.0.2
Django Rest Framework 3.7.7
asyncio
zipfile

# Requirements
* PIP3 must be set up

# Deployment
1. Clone the project.
2. Create virtual environment in the root `virtualenv venv`
3. Activate the virtualenv and install all the dependencies from the requirements.txt file
4. Make migrations to the database by `python manage.py migrate` for Windows or `./manage.py migrate` for Linux
5. Run Django server by `python manage.py runserver` for Windows or `./manage.py runserver` for Linux

# Support
* Should work on Linux and Windows (Last tested on Windows).
* When hosting on Windows OS there is an issue with the CSRF tokens. It seems fixed now, but in case of problems, disable the CSRF middleware.
* On Windows, antivirus programs might interfere.